package com.Assignment1;

public class Pom {

	private static String element = null;
//		 private static List<WebElement> ele = null;

	public String searchBox() {

		element = "//input[@id='twotabsearchtextbox']";
		return element;

	}

	public String clickOnSearch() {

		element = "//*[@type='submit']";
		return element;
	}

	public String iPhoneXRPrice() {

		element = "//*[@class='s-result-list s-search-results sg-row']/div[1]/div/span/div/div/div[2]/div[2]/div/div[2]/div[1]/div/div[1]/div/div/a/span[1]";
		return element;
	}

	public String searchBoxFlipkart() {

		element = "//input[@placeholder='Search for products, brands and more']";
		return element;
	}

	public String iPhoneXRPriceinFlipkart() {

		element = "//*[@data-id='MOBF9Z7ZUGQ6YDBH']/div/a/div[2]/div[2]/div[1]/div/div[1]";
		return element;
	}

	public String loginClose() {

		element = "//button[@class='_2AkmmA _29YdH8']";
		return element;
	}

	public String search() {

		element = "//*[@title='Search']";
		return element;
	}

	public String search2() {

		element = "//*[@type='Search']";
		return element;
	}

	public String searchMAin() {

		element = "//*[@id='mainSearch']";
		return element;
	}

	public String searchbuttom() {

		element = "//*[@type='submit']";
		return element;
	}

	public String search1() {

		element = "//*[@class='input_box activeInput']";
		return element;
	}

	public String listHotel() {

		element = "//*[@class='ui_column is-9-desktop is-8-mobile is-9-tablet content-block-column']";
		return element;
	}

	public String review() {

		element = "//*[@class='hotels-hotel-review-atf-info-parts-Rating__reviewCount--1sk1X']";
		return element;
	}

	public String writeReview() {

		element = "//*[@class='ui_button primary']";
		return element;
	}

	public String HoverbubbleRate() {

		element = "//*[@id='bubble_rating']";
		return element;
	}

	public String reviewTitle() {

		element = "//*[@id='ReviewTitle']";
		return element;
	}

	public String yourReview() {

		element = "//*[@id='ReviewText']";
		return element;
	}
	
	public String hotelRatings() {

		element = "//div[contains(text(),'Hotel Ratings')]";
		return element;
	}
	

	public String service() {

		element = "//span[@id='qid12_bubbles']";
		return element;
	}

	public String cleanliness() {

		element = "//span[@id='qid14_bubbles']";
		return element;
	}

	public String sleepQuality() {

		element = "//span[@id='qid190_bubbles']";
		return element;
	}

	public String location() {

		element = "//span[@id='qid47_bubbles']";
		return element;
	}
	
	public String rooms() {

		element = "//span[@id='qid11_bubbles']";
		return element;
	}
	public String value() {

		element = "//span[@id='qid13_bubbles']";
		return element;
	}
	
	public String checkBox() {

		element = "//input[@id='noFraud']";
		return element;
	}

	
}
