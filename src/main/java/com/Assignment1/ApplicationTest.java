package com.Assignment1;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utilitys.BrowserFactory;
import utilitys.ConstantPath;

public class ApplicationTest {

	static WebDriver driver;
	static WebElement element;

	/******
	 * @preetish
	 *******/

	/*** Chrome ***/
	@Parameters({ "browser" })
	@BeforeClass(alwaysRun = true)
	public void startChromeBrowser(String browser) throws Exception {
		try {
			driver = BrowserFactory.Setup_Grid(browser, ConstantPath.VersionControlurl);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(alwaysRun = true)
	public void priceCompare() throws Throwable {

		ComparePrice test = new ComparePrice();
		test.searchiPhoneAmazon(BrowserFactory.uAgent, driver, element);
		test.searchiPhonePriceInFlipkart(BrowserFactory.uAgent, driver, element);
		ComparePrice.compareValue();
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}