package com.Assignment1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import core.WaitFor;

public class ComparePrice {

	Pom PomObj;
	static String amazonPrice;
	static String flipkartPrice;
	
	public String searchiPhoneAmazon(String uAgent, WebDriver driver, WebElement element) throws Exception {

		String price = null;

		try {
			System.out.println("BrowerName-->" + uAgent);

			PomObj = new Pom();
			WaitFor.presenceOfElementByXpath(driver, PomObj.searchBox());
			element = driver.findElement(By.xpath(PomObj.searchBox()));

			element.click();
			element.sendKeys("iPhone XR (64GB) - Yellow");

			element = driver.findElement(By.xpath(PomObj.clickOnSearch()));
			element.click();

			element = driver.findElement(By.xpath(PomObj.iPhoneXRPrice()));
			price = element.getText();
			
			amazonPrice = price.replaceAll("[^a-zA-Z0-9]", "");
			
			System.out.println("Amazon iPhone XR (64GB)-Yellow price-->"+amazonPrice);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return amazonPrice;
	}
	
	
	
	
	public String searchiPhonePriceInFlipkart(String uAgent, WebDriver driver, WebElement element) throws Exception {

		String price = null;
		
		try {
			
			driver.get("https://www.flipkart.com");
			
			PomObj = new Pom();
			
			WaitFor.presenceOfElementByXpath(driver, PomObj.loginClose());
			element = driver.findElement(By.xpath(PomObj.loginClose()));
			element.click();
					
			WaitFor.presenceOfElementByXpath(driver, PomObj.searchBoxFlipkart());
			element = driver.findElement(By.xpath(PomObj.searchBoxFlipkart()));

			element.click();
			element.sendKeys("iPhone XR (64GB) - Yellow");

			element = driver.findElement(By.xpath(PomObj.clickOnSearch()));
			element.click();

			Thread.sleep(5000);
			
			WaitFor.presenceOfElementByXpath(driver, PomObj.iPhoneXRPriceinFlipkart());
			element = driver.findElement(By.xpath(PomObj.iPhoneXRPriceinFlipkart()));
			price = element.getText();
			
			flipkartPrice = price.replaceAll("[^a-zA-Z0-9]", "");
			
			System.out.println("FlipKart iPhone XR (64GB)-Yellow price--> "+flipkartPrice);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flipkartPrice;

	}
	
	
	public static void compareValue() {
		
		try {
			int amazon = Integer.parseInt(amazonPrice.trim());
			int flipkart =Integer.parseInt(flipkartPrice.trim());
					
			if(amazon < flipkart) {		
				System.out.println("Amazon price is less than Flipkart");
			}else {		
				System.out.println("Flipkart price is less than Amazon");
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}
	

}
