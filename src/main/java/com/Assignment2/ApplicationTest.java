package com.Assignment2;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utilitys.BrowserFactory;
import utilitys.ConstantPath;

public class ApplicationTest {

	static WebDriver driver;
	static WebElement element;
	

	/******
	 * @Preetish
	 *******/

	/*** Chrome ***/
	@Parameters({ "browser" })
	@BeforeClass(alwaysRun = true)
	public void startChromeBrowser(String browser) throws Exception {
		try {
			driver = BrowserFactory.Setup_Grid(browser, ConstantPath.url);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(alwaysRun = true)
	public void test01() throws Throwable {

		Trip test = new Trip();
		test.SearchClubMahindra(BrowserFactory.uAgent, driver, element);
	}

	
	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}