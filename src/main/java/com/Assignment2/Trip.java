package com.Assignment2;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.Assignment1.Pom;
import core.Slidemouse;
import core.SwitchWindow;
import core.WaitFor;

public class Trip {

	Pom PomObj;

	public void SearchClubMahindra(String uAgent, WebDriver driver, WebElement element) throws Exception {

		try {
			System.out.println("BrowerName-->" + uAgent);

			PomObj = new Pom();

			try {
				WaitFor.presenceOfElementByXpath(driver, PomObj.search());
				element = driver.findElement(By.xpath(PomObj.search()));

			} catch (Exception e) {

				WaitFor.presenceOfElementByXpath(driver, PomObj.search2());
				element = driver.findElement(By.xpath(PomObj.search2()));
			}

			element.click();

			try {
				Thread.sleep(4000);
				element.sendKeys("Club Mahindra");

			} catch (Exception e) {

				WaitFor.presenceOfElementByXpath(driver, PomObj.searchMAin());
				element = driver.findElement(By.xpath(PomObj.searchMAin()));
				Thread.sleep(4000);
				element.click();
				element.sendKeys("Club Mahindra");
			}

			try {
				WaitFor.presenceOfElementByXpath(driver, PomObj.searchbuttom());
				element = driver.findElement(By.xpath(PomObj.searchbuttom()));
				element.click();

			} catch (Exception e) {
				Thread.sleep(3000);
				Robot r = new Robot();
				r.delay(10);
				r.keyPress(KeyEvent.VK_ENTER);
				r.keyRelease(KeyEvent.VK_ENTER);
			}

			WaitFor.presenceOfElementByXpath(driver, PomObj.listHotel());
			List<WebElement> listhst = driver.findElements(By.xpath(PomObj.listHotel()));

			for (WebElement ele1 : listhst) {

				ele1.click();

				break;
			}

			SwitchWindow.switchtab(driver, 1);

			WaitFor.presenceOfElementByXpath(driver, PomObj.review());
			element = driver.findElement(By.xpath(PomObj.review()));
			element.click();

			WaitFor.presenceOfElementByXpath(driver, PomObj.writeReview());
			element = driver.findElement(By.xpath(PomObj.writeReview()));
			element.click();

			SwitchWindow.switchtab(driver, 2);

			element = driver.findElement(By.xpath(PomObj.HoverbubbleRate()));
			Slidemouse.slideRight(element, driver, 100);

			element = driver.findElement(By.xpath(PomObj.reviewTitle()));
			element.click();
			element.sendKeys("Nice place");

			element = driver.findElement(By.xpath(PomObj.yourReview()));
			element.click();
			element.sendKeys("I had visited this place with my family for a holiday. Overall had a good experience.");

			element = driver.findElement(By.xpath(PomObj.hotelRatings()));
			String value = element.getText();

			if (value.contains("Hotel Ratings")) {
				try {
					System.out.println(value + "section is available. Repeating step 5. ");

					element = driver.findElement(By.xpath(PomObj.service()));
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
					Slidemouse.slideRight(element, driver, 80);

					element = driver.findElement(By.xpath(PomObj.location()));
					Slidemouse.slideRight(element, driver, 80);

					element = driver.findElement(By.xpath(PomObj.rooms()));
					Slidemouse.slideRight(element, driver, 80);

					element = driver.findElement(By.xpath(PomObj.cleanliness()));
					Slidemouse.slideRight(element, driver, 80);

					element = driver.findElement(By.xpath(PomObj.value()));
					Slidemouse.slideRight(element, driver, 80);

					element = driver.findElement(By.xpath(PomObj.sleepQuality()));
					Slidemouse.slideRight(element, driver, 80);
				} catch (Exception e) {

				}
			}

			element = driver.findElement(By.xpath(PomObj.checkBox()));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
			element.click();
			System.out.println("Successfully Submitted the review");
			
			Thread.sleep(4000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
