package utilitys;

public class ThreadHelper {

	public static void sleepForSec(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
