package utilitys;

import java.io.File;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeDriverService;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BrowserFactory {

	public static WebDriver driver;
	static DesiredCapabilities capabilities = null;
	static Capabilities cap;
	public static String uAgent;

	public BrowserFactory(WebDriver driver) {
		BrowserFactory.driver = driver;

	}

	@SuppressWarnings("deprecation")
	@BeforeClass
	@Parameters({ "browser" })

	public static WebDriver Setup_Grid(String browser, String url) throws Exception {

		try {
			if (browser.equalsIgnoreCase("firefox")) {

				WebDriverManager.firefoxdriver().setup();
				FirefoxOptions options = new FirefoxOptions();
				driver = new FirefoxDriver(options);
	
				uAgent = (String) ((JavascriptExecutor) driver)
						.executeScript("return typeof InstallTrigger !== 'undefined'?'firefox':''");

				System.out.println(uAgent);
		

			}

			else if (browser.equalsIgnoreCase("chrome")) {

				WebDriverManager.chromedriver().setup();
				driver = new ChromeDriver();

				uAgent = (String) ((JavascriptExecutor) driver).executeScript(
						"return navigator.userAgent.match(/Chrom(e|ium)\\/([0-9]+)\\./)? 'chrome' : '';");

				driver.manage().window().maximize();
			}

			else if (browser.equalsIgnoreCase("edge")) {

				capabilities = DesiredCapabilities.edge();
				capabilities.getBrowserName();
				capabilities.setJavascriptEnabled(true);
				capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				capabilities.setCapability(EdgeDriverService.EDGE_DRIVER_EXE_PROPERTY,
						System.setProperty("webdriver.edge.driver", "./driver\\MicrosoftWebDriver.exe"));

				driver = new EdgeDriver(capabilities);

				driver.manage().window().maximize();

			}

			else if (browser.equalsIgnoreCase("ie")) {

				// System.setProperty("webdriver.ie.driver",DataProviderFactory.getConfig().getIEPath());

				capabilities = DesiredCapabilities.internetExplorer();
				capabilities.setCapability(InternetExplorerDriverService.IE_DRIVER_EXE_PROPERTY,
						System.setProperty("webdriver.ie.driver", DataProviderFactory.getConfig().getIEPath()));
				// capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
				capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				capabilities.setJavascriptEnabled(true);
				driver = new InternetExplorerDriver(capabilities);

				driver.manage().window().maximize();
			}

			else if (browser.equalsIgnoreCase("safari")) {

				driver = new SafariDriver();

			}

			else if (browser.equalsIgnoreCase("opera")) {

				System.setProperty("webdriver.opera.driver", "./driver\\operadriver.exe");

				OperaOptions options = new OperaOptions();
				options.setBinary(new File("C:\\Program Files\\Opera\\launcher.exe"));

				capabilities = DesiredCapabilities.operaBlink();
				capabilities.setBrowserName("opera");
				capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				capabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);

				browser = capabilities.getBrowserName();
				System.out.println(browser);


				driver = new OperaDriver(options);

				driver.manage().window().maximize();

			}

			driver.manage().timeouts().setScriptTimeout(40, TimeUnit.SECONDS);

			// driver.manage().deleteAllCookies();
			// ((JavascriptExecutor)driver).executeScript("window.location = \'"+url+"\'");
			driver.get(url);

			new WebDriverWait(driver, 20).until(WebDriver -> ((JavascriptExecutor) WebDriver)
					.executeScript("return document.readyState").equals("complete"));

			Thread.sleep(3000);

		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return driver;

	}
}