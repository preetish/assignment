package utilitys;

import org.openqa.selenium.WebDriver;

public class Switch{
	
	WebDriver driver;
	
	
	public Switch(WebDriver driver) {
	     this.driver=driver;
	}
	
	
	public void SwitchCase(String uAgent) throws InterruptedException {
		
		Thread.sleep(6000);
		
		switch (uAgent) {
		
		
		case "IE":
			driver.switchTo().frame(0);
			break; 

		case "edge":
			driver.switchTo().frame(0);
			break;     

		case "opera":
			driver.switchTo().frame(0);
			break;           

		case "chrome":
			driver.switchTo().frame(0);
			break;	

		case "firefox":
			driver.switchTo().frame(3);
			break;           
		}
			
	}

}
