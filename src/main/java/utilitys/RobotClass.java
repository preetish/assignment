package utilitys;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

public class RobotClass {

	public static void end() throws InterruptedException, AWTException {

		Robot r = new Robot();
		Thread.sleep(3000);
		r.keyPress(KeyEvent.VK_END);
		r.keyRelease(KeyEvent.VK_END);
	}

	public static void delete() throws InterruptedException, AWTException {

		Robot r = new Robot();
		Thread.sleep(3000);
		r.keyPress(KeyEvent.VK_DELETE);
		r.keyRelease(KeyEvent.VK_DELETE);

	}
	
	
	public static void home() throws InterruptedException, AWTException {

		Robot r = new Robot();
		Thread.sleep(3000);
		r.keyPress(KeyEvent.VK_HOME);
		r.keyRelease(KeyEvent.VK_HOME);

	}
	
	@SuppressWarnings("deprecation")
	public static void mouse() throws AWTException {
		Robot robot = new Robot();
		robot.mousePress(InputEvent.BUTTON1_MASK);
	    robot.mouseRelease(InputEvent.BUTTON1_MASK);
	}
	
	public static void shiftdown() throws InterruptedException, AWTException {

		Robot r = new Robot();
		Thread.sleep(3000);
		r.keyPress(KeyEvent.VK_SHIFT);

	}	
	
	public static void shiftup() throws InterruptedException, AWTException {

		Robot r = new Robot();
		Thread.sleep(3000);
		r.keyRelease(KeyEvent.VK_SHIFT);

	}	
	
	

	public static void backspace() throws InterruptedException, AWTException {

		Robot r = new Robot();
		Thread.sleep(3000);
		r.keyPress(KeyEvent.VK_BACK_SPACE);
		r.keyRelease(KeyEvent.VK_BACK_SPACE);

	}

	public static void arrowLeft() throws InterruptedException, AWTException {

		Robot r = new Robot();
		r.delay(20);
		r.keyPress(KeyEvent.VK_LEFT);
		r.keyRelease(KeyEvent.VK_LEFT);
	}

	public static void arrowRight() throws InterruptedException, AWTException {

		Robot r = new Robot();
		r.delay(20);
		r.keyPress(KeyEvent.VK_RIGHT);
		r.keyRelease(KeyEvent.VK_RIGHT);

	}

	public static void selectShitHomeBckSpace() throws AWTException, InterruptedException {

		Robot r = new Robot();
		Thread.sleep(1000);
		r.keyPress(KeyEvent.VK_SHIFT);
		r.keyPress(KeyEvent.VK_HOME);
		r.keyRelease(KeyEvent.VK_SHIFT);
		r.delay(50);
		r.keyPress(KeyEvent.VK_BACK_SPACE);
		r.keyRelease(KeyEvent.VK_BACK_SPACE);
	}

	public static void shihtHomedeleteKey() throws AWTException, InterruptedException {

		Robot r = new Robot();
		Thread.sleep(1000);
		r.keyPress(KeyEvent.VK_SHIFT);
		r.keyPress(KeyEvent.VK_HOME);
		r.keyRelease(KeyEvent.VK_SHIFT);
		r.delay(50);
		r.keyPress(KeyEvent.VK_DELETE);
		r.keyRelease(KeyEvent.VK_DELETE);

	}
	
	public static void pressShiftHome() throws AWTException, InterruptedException {

		Robot r = new Robot();
		Thread.sleep(1000);
		r.keyPress(KeyEvent.VK_SHIFT);
		r.keyPress(KeyEvent.VK_HOME);
		r.keyRelease(KeyEvent.VK_SHIFT);
		
	}
	
	public static void selectTextCopy() throws InterruptedException, AWTException {

		Robot r = new Robot();
		Thread.sleep(1000);
		r.keyPress(KeyEvent.VK_SHIFT);
		r.keyPress(KeyEvent.VK_HOME);
		r.keyRelease(KeyEvent.VK_SHIFT);
		r.delay(50);
		r.keyPress(KeyEvent.VK_CONTROL);
		r.keyPress(KeyEvent.VK_V);
		r.keyRelease(KeyEvent.VK_V);
		r.keyRelease(KeyEvent.VK_CONTROL);
	}
	
	public static void paste() throws InterruptedException, AWTException {

		Robot r = new Robot();
		Thread.sleep(1000);
		r.keyPress(KeyEvent.VK_PASTE);
		r.keyRelease(KeyEvent.VK_PASTE);

	}

}