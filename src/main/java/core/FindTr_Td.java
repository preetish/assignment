package core;

public class FindTr_Td {

	static int rowCount = 3, columnCount = 4;
	final static int selectedCell = 8;

	public static void main(String[] args) {
		System.out.println("Row : " + getTableRowNo());
		System.out.println("Column : " + getTableDivisionNo());
	}

	private static int getTableRowNo() {
		int trNo = 1;
		int tempColumn = columnCount;
		while (selectedCell > tempColumn) {
			tempColumn += columnCount;
			trNo++;
		}
		return trNo;
	}

	private static int getTableDivisionNo() {
		int tdNo = 1, startIndex, endIndex;
		endIndex = columnCount * getTableRowNo();
		startIndex = endIndex - columnCount + 1;

		for (int i = startIndex; i < endIndex; i++) {
			if (i == selectedCell) {
				break;
			} else {
				tdNo++;
			}
		}
		return tdNo;
	}
}
