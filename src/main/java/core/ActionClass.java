package core;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class ActionClass {

	WebDriver driver;
	WebElement element;

	
	public ActionClass(WebDriver driver,WebElement element) {
		this.driver= driver;
		this.element=element;
	}
	

	public void cut() throws InterruptedException {

		Actions action = new Actions(driver);
		Thread.sleep(1000);
		action.keyDown(Keys.CONTROL).build().perform();
		action.sendKeys("x").build().perform();
		Thread.sleep(1000);
		action.keyUp(Keys.CONTROL).build().perform();
		Thread.sleep(1000);
	}

	public void click() throws InterruptedException {

		Actions action = new Actions(driver);
		action.click(element);

	}

	public void paste() throws InterruptedException {

		Actions action = new Actions(driver);
		Thread.sleep(1000);
		action.keyDown(Keys.CONTROL).build().perform();
		Thread.sleep(1000);
		action.sendKeys("v").build().perform();
		Thread.sleep(1000);
		action.keyUp(Keys.CONTROL).build().perform();
		Thread.sleep(1000);
		
	}

	public void end() throws InterruptedException {

		Actions action = new Actions(driver);
		Thread.sleep(2000);
		action.sendKeys(Keys.END).build().perform();
	}
	
	public void delete() throws InterruptedException {

		Actions action = new Actions(driver);
		Thread.sleep(2000);
		action.sendKeys(Keys.DELETE).build().perform();
	}



	public void right() throws InterruptedException {

		Actions action = new Actions(driver);
		action.sendKeys(Keys.ARROW_LEFT).build().perform();
	}
	
	public void copy() throws InterruptedException {

		Actions action = new Actions(driver);
		action.keyDown(Keys.CONTROL).build().perform();
		Thread.sleep(1000);
		action.sendKeys("c").build().perform();
		Thread.sleep(500);
		action.keyUp(Keys.CONTROL).build().perform();

	}
}
