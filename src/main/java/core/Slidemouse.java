package core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class Slidemouse {

	static Actions SliderAction;
	public static void slideRight(WebElement element, WebDriver driver, int nbr) throws InterruptedException  {

		try {
			
			org.openqa.selenium.Dimension dim = element.getSize();
			int x = dim.getWidth();
			SliderAction = new Actions(driver);
			Thread.sleep(2000);
			SliderAction.dragAndDropBy(element, x - nbr, 0).click().release().perform();
			
		} catch (Exception e) {
			
			
		}
	}
}