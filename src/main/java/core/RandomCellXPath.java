package core;

import java.util.List;
import java.util.Random;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class RandomCellXPath {

	static Random sRandom = new Random();
	private WebDriver mDriver;
	private String mBaseXpath;
	private int rowCount = 0, maxColumnCount = 0;

	public RandomCellXPath(WebDriver driver, String baseXpath) {
		mDriver = driver;
		mBaseXpath = baseXpath;
		getRowColumnCount();
	}

	private void getRowColumnCount() {

		WebElement tableElement = mDriver.findElement(By.xpath(mBaseXpath));

		List<WebElement> tempRows = tableElement.findElements(By.tagName("tr"));

		rowCount = tempRows.size();

		for (WebElement tempRow : tempRows) {
			List<WebElement> tempColumn = tempRow.findElements(By.tagName("td"));
			maxColumnCount = tempColumn.size() > maxColumnCount ? tempColumn.size() : maxColumnCount;
		}

	}

	public WebElement getElement() {
		WebElement ranCellElement = null;

		String ranCellXPath;
		int tempRowNo = 0, tempColumnNo = 0;
		boolean sholdLoop = true;

		do {
			
			ranCellXPath = mBaseXpath + "//tr[%d]//td[%d]";
			tempRowNo = getRandom(rowCount);
			tempColumnNo = getRandom(maxColumnCount);
			System.out.println(tempRowNo + "<->" + tempColumnNo);

			try {
				
				ranCellXPath = String.format(ranCellXPath, tempRowNo, tempColumnNo);
				System.out.println("ranCellXPath : " + ranCellXPath);
				ranCellElement = mDriver.findElement(By.xpath(ranCellXPath));

				System.out.println("Text is : " + ranCellElement.getText());
				
				sholdLoop = ranCellElement.getText().trim().isEmpty();
				
				
				if(ranCellElement.getAttribute("innerHTML").contains("del")) {
					sholdLoop=ranCellElement.getAttribute("innerHTML").contains("del");
				}
				
				if(ranCellElement.getAttribute("innerHTML").contains("ins")) {
					sholdLoop=ranCellElement.getAttribute("innerHTML").contains("ins");
				}
				
			} catch (NoSuchElementException nee) {

			}

		} while (sholdLoop);

		return ranCellElement;

	}

	private int getRandom(int range) {

		int number = sRandom.nextInt(range);
		if (number == 0) {
			getRandom(range);
		}
		return number;
	}
}
