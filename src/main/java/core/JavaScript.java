package core;

import java.util.NoSuchElementException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class JavaScript {
	public static WebDriver driver;

	public JavaScript(WebElement element) {

	}

	public static JavaScript JsClickTheElement(final WebDriver driver, final WebElement ele) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", ele);
		return new JavaScript(ele);
	}

	public static JavaScript JsEditInnerHTML(final WebDriver driver, final WebElement ele) {
		((JavascriptExecutor) driver).executeScript("var ele=arguments[0]; ele.innerHTML = 'Access probabil‌ities.';",
				ele);
		if (ele.getText().contains("Access probabilities."))

			return new JavaScript(ele);
		;
		return null;
	}

	public static JavaScript JsRefresh(final WebDriver driver) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("history.go(0)");
		return JsRefresh(driver);
	}

	public static void JsForSelectTheWord(final WebDriver driver) {
		String jscript = "var text = 'Computerised task'; if (window.find && window.getSelection) {document.designMode = 'on';var sel = window.getSelection();sel.collapse(document.body, 0);window.find(text);document.designMode = 'off';} else if (document.body.createTextRange) {var textRange = document.body.createTextRange();textRange.findText(text)}";
		((JavascriptExecutor) driver).executeScript(jscript);
	}

	public static void JsRightClick(final WebDriver driver, final WebElement ele) {
		JavascriptExecutor js = (JavascriptExecutor) driver;

		String javaScript = "var evt = document.createEvent('MouseEvents');" + "var RIGHT_CLICK_BUTTON_CODE = 2;"
				+ "evt.initMouseEvent('contextmenu', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, RIGHT_CLICK_BUTTON_CODE, null);"
				+ "arguments[0].dispatchEvent(evt)";

		js.executeScript(javaScript, ele);

	}

	public static void safeJavaScriptClick(final WebElement element, final WebDriver driver) throws Throwable {
		try {
			if (element.isEnabled() && element.isDisplayed()) {
				System.out.println("Clicking on element with using java script click");

				((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
			} else {
				System.out.println("Unable to click on element");
			}
		} catch (StaleElementReferenceException e) {
			System.out.println("Element is not attached to the page document " + e.getStackTrace());
		} catch (NoSuchElementException e) {
			System.out.println("Element was not found in DOM " + e.getStackTrace());
		} catch (Throwable e) {
			System.out.println("Unable to click on element " + e.getStackTrace());
		}
	}

	public void mouseHoverJScript(WebElement HoverElement) {
		try {

			if (isElementPresent(HoverElement)) {

				String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";

				((JavascriptExecutor) driver).executeScript(mouseOverScript, HoverElement);

			} else {
				System.out.println("Element was not visible to hover " + "\n");

			}
		} catch (StaleElementReferenceException e) {
			System.out.println(
					"Element with " + HoverElement + "is not attached to the page document" + e.getStackTrace());
		} catch (NoSuchElementException e) {
			System.out.println("Element " + HoverElement + " was not found in DOM" + e.getStackTrace());
		}
	}

	public static boolean isElementPresent(WebElement element) {
		boolean flag = false;
		try {
			if (element.isDisplayed() || element.isEnabled())
				flag = true;
		} catch (NoSuchElementException e) {
			flag = false;
		} catch (StaleElementReferenceException e) {
			flag = false;
		}
		return flag;
	}

	public static WebElement doubleClick(WebElement ele) {
		String doubleClickJS = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('dblclick',"
				+ "true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject)"
				+ "{arguments[0].fireEvent('ondblclick');}";

		((JavascriptExecutor) driver).executeScript(doubleClickJS, ele);

		return ele;
	}

	public static String RemoveTag(String classname) {

		String tagRemove = "var x = document.getElementsByTagName('"+classname+"');" 
				+ "alert(x.length);"
				+ " for(i =0;i<x.length;i++){"
				+"console.log(x[i]);"
		        + "x[i].remove();" + "  }" 
				+ "alert(x.length);";
			
		((JavascriptExecutor) driver).executeScript(tagRemove);

		return classname;
	}
}
